#ifndef SOCKETS_H
#define SOCKETS_H

int init_socket(char* name);
void socket_client_conn(int socket, char* path, char* name);
void socket_server_bind(int socket, char* path, char* name);
int socket_server_wait_for_connection(int socket, int possible_connections, char* name);
int blocking_write_to_socket(int socket, char* msg, int msg_size);
int blocking_read_from_socket(int socket, char* buf, int buf_size);
void close_socket_conn(int socket, char* name);
void close_server_socket(int socket, char* name);

#endif
