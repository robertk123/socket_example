// klient

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#include "sockets.h"

int main( int argc, char ** argv )
{
    if( argc != 2 ) 
    {
        printf("Potrzeba 1 argumentu!\n");
        exit(-1);
    }

    int fd = init_socket("klient");

    socket_client_conn(fd, "/tmp/test_socket", "klient");

    blocking_write_to_socket(fd, argv[1], strlen(argv[1]));

    close_socket_conn(fd, "klient");
}

