/*
 * sockets.c
 *
 *  Created on: May 24, 2019
 *      Author: Fixed
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/un.h>
#include <stdio.h>

#include "sockets.h"

#define ERROR(msg, ...) fprintf(stderr, msg "\n", __VA_ARGS__)

int init_socket( char * name ) {
	int sockfd;
	struct sockaddr_un;
	if ((sockfd = socket(AF_UNIX,SOCK_STREAM,0)) < 0) 
        {
            ERROR("Failed to create %s socket", name); 
            exit(-1);
	}

	return sockfd;
}

void socket_client_conn(int socket, char* path, char* name) 
{
	int servlen;
	struct sockaddr_un  serv_addr;
	bzero((char *)&serv_addr,sizeof(serv_addr));
	serv_addr.sun_family = AF_UNIX;
	strcpy(serv_addr.sun_path, path);
	servlen = strlen(serv_addr.sun_path) +
				 sizeof(serv_addr.sun_family);
	if (connect(socket, (struct sockaddr *)&serv_addr, servlen) < 0) 
        {
	    ERROR("Error while connecting to %s socket", name);
	}
}

void socket_server_bind(int socket, char* path, char* name) 
{
	int servlen;
	struct sockaddr_un  serv_addr;
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sun_family = AF_UNIX;
	strcpy(serv_addr.sun_path, path);
	servlen=strlen(serv_addr.sun_path) +
					 sizeof(serv_addr.sun_family);
	
	unlink(path);
	
	if(bind(socket,(struct sockaddr *)&serv_addr,servlen)<0) {
		ERROR("Error while binding %s socket", path);
	}
}

int socket_server_wait_for_connection(int socket, int possible_connections,
		char* name) {
	struct sockaddr_un  cli_addr;
	socklen_t clilen;
	listen(socket,possible_connections);
	clilen = sizeof(cli_addr);
	int new_socket = accept(
		socket,(struct sockaddr *)&cli_addr,&clilen);
	if (new_socket < 0) {
	    	ERROR("Error while accepting %d socket connection", socket);
	}

	return new_socket;
}

int blocking_write_to_socket(int socket, char* msg, int msg_size) {
	int n = write(socket, msg, msg_size);
	return n;
}

int blocking_read_from_socket(int socket, char* buf, int buf_size) {
	int n = read(socket, buf, buf_size);
	return n;
}

void close_socket_conn(int socket, char* name){
	close(socket);
}
void close_server_socket(int socket, char* name) {
	close(socket);
}
