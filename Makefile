CC = gcc
CFLAGS = -std=gnu99 -Wall -Iinclude -DPLATFORM=`uname | sed 's/[^a-zA-Z0-9_].*//'`

RECIVER_OBJECTS = build/sockets.o build/reciver.o 
SENDER_OBJECTS =  build/sockets.o build/sender.o

COMMON_HEADERS = sockets.h 

all: sender reciver 

build/%.o: %.c
	@mkdir -p build
	$(CC) $(CFLAGS) -c $< -o $@

reciver: $(RECIVER_OBJECTS) $(COMMON_HEADERS)
	@mkdir -p bin
	$(CC) $(CFLAGS) $(RECIVER_OBJECTS) -o reciver

sender: $(SENDER_OBJECTS) $(COMMON_HEADERS)
	@mkdir -p bin
	$(CC) $(CFLAGS) $(SENDER_OBJECTS) -o sender

clean:
	rm -r build/  bin/
