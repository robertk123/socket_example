// serwer

#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include "sockets.h"

void proces(uint8_t type, char * msg )
{
	if( msg )
		printf("%d: %s\n", type, msg);
	else
		printf("%d: none\n", type);
}

int main( void )
{
	int serwer = init_socket("serwer");
	socket_server_bind(serwer, "/tmp/test_socket", "serwer");

	char dane[100] = {0};

	while(2)
	{
		int klient = socket_server_wait_for_connection(serwer, 3, "klient");

		int len = blocking_read_from_socket(klient, dane, sizeof(dane));

		fprintf(stderr, "Odebrano[%d]: \"%s\"\n", len, dane);
	
		close_server_socket(klient, "klient");
		bzero(dane, sizeof(dane));	
	}
}

